import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

import {ContactService} from '../contact.srv';
import {Contact} from "../contact.cls";

@Component({
    templateUrl: 'app/edit/edit.html'
})
export class EditComponent implements OnInit {
    contact: Contact = new Contact("","","");

    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactService: ContactService) {}

    back() {
        this.router.navigateByUrl('/search');
    }

    editContact(): void {
        this.contactService.editContact(this.contact)
            .then(() => {
                this.back();
            });
    }

    ngOnInit(): void {
        const id  = this.route.snapshot.paramMap.get('contact_id')

        this.contactService.getContact(id)
            .then(contact => this.contact = contact);
    }

}
