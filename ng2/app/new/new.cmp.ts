import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

import {ContactService} from '../contact.srv';
import { NewContact } from "../contact.cls";

@Component({
    templateUrl: 'app/new/new.html'
})
export class NewComponent{
    newContact: NewContact = new NewContact("","");

    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactService: ContactService) {}

    back() {
        this.router.navigateByUrl('/search');
    }

    addNewContact(): void {
        this.contactService.saveContact(this.newContact)
            .then(() => {
            this.back();
            });

    }

}
