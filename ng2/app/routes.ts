import { Routes } from '@angular/router';

import { SearchComponent } from './search/search.cmp';
import { EditComponent } from './edit/edit.cmp';
import { NewComponent } from "./new/new.cmp";

export const routes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'edit/:contact_id', component: EditComponent },
    { path: 'new', component: NewComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];