'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const Dao = require('./dao.js');
const conf = require('./conf.json');
const dbUser = conf.user;
const dbPass = conf.password;
const url = 'mongodb://' + dbUser + ':' + dbPass + '@ds249355.mlab.com:49355/i399';
const app = express();
app.use(express.static('./'));
app.use(bodyParser.json());


app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', findById);
app.delete('/api/contacts/:id', deleteId);
app.post('/api/contacts', insert);
app.post('/api/contacts/delete', deleteMany);
app.put('/api/contacts/:id', updateId);

app.use(errorHandler);

app.listen(3000, () => console.log('Server is running on port 3000'));

var dao = new Dao();
dao.connect(url);

function getContacts(request, response, next) {
    dao.findAll()
        .then(contacts => {
            response.json(contacts);
        }).catch(next);
}

function findById(request, response, next) {
    var id = request.params.id;
    dao.findById(id)
        .then(contact => {
            response.json(contact);
        }).catch(next);
}

function deleteId(request, response, next) {
    var id = request.params.id;
    dao.deleteId(id)
        .then(contact => {
            response.end();
        }).catch(next);
}

function insert(request, response, next) {
    var data = request.body;
    dao.insert(data,dao.getNewId())
        .then(contact=> {
            response.end();
        }).catch(next);
}

function updateId(request, response, next) {
    var data = request.body;
    var id = request.params.id;
    dao.updateId(id,data)
        .then(contact=> {
            response.end();
        }).catch(next);
}

function deleteMany(request, response, next) {
    var data = request.body;
    dao.deleteMany(data)
        .then(contact=> {
            response.end();
        }).catch(next);
}

function errorHandler(error, request, response, next) { // there must be 4 arguments
    response.status(404).json({ error: error.toString()});
}
