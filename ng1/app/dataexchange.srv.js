(function() {
  'use strict';

  angular.module('app').service('dataExchange', Exchange);

  function Exchange() {
    
    var data = {};

    var setData= function (recieved) {
      
      data=recieved;
      
    };
    
    var getData = function(){
      return data;
    };
    
    return {
      setData: setData,
      getData: getData
    };

  }

})();