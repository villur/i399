(function() {
  'use strict';

  angular.module('app').controller('editCtrl', Edit);

  function Edit($http,$window,dataExchange) {

    var vm = this;

    vm.item = dataExchange.getData();
    
    console.log(vm.item);

   

    this.save = function(item) {
      var editFull = {
        _id: vm.item._id,
        name: vm.item.name,
        phone: vm.item.phone
      }
        $http.put('api/contacts/'+vm.item._id, editFull).then(function(result) {
          $window.location = "#/search"
        });
      

      
    }

  }

})();