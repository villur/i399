'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'Contacts';

class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    findAll() {
        return Promise.resolve(this.db.collection(COLLECTION)
        .find()
        .toArray());
    }



    findById(id){
        return this.db.collection(COLLECTION)
        .findOne({ _id: new ObjectID(id) });
    }

    
    insert(data){
        return this.db.collection(COLLECTION)
            .insertOne(data);
    }

    updateId(id,data){
        data._id = new ObjectID(id);
        return this.db.collection(COLLECTION)
        .updateOne({ _id: new ObjectID(id) }, data);
    }

    deleteId(id){
        return this.db.collection(COLLECTION)
        .deleteOne({ _id: new ObjectID(id) });
    }
    
    deleteMany(data){
        return this.db.collection(COLLECTION)
        .remove({ _id : { $in : data
            .map(id => new ObjectID(id))}});
    }
    

    getNewId() {
        return (Math.random() + 'A').substr(2);
    }

    close() {
            if (this.db) {
                this.db.close();
            }
        }
}

module.exports = Dao;
